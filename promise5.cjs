// import the functions from three different files
const fngetBoardById = require("./promise1.cjs");
const fngetListById = require("./promise2.cjs");
const fngetCardsBylist= require("./promise3.cjs");

//import fs and path module
const fs = require("fs");
const path = require("path");

//define the function w
function getBoardListandcards() {
  let thanosId;
  let thanosLists;
  // let mindListId;

  return new Promise((resolve,reject) => {
      fngetBoardById("mcu453ed")
      .then((board) => {
          if (!board) {
              console.log("Thanos board not found");
          }

          thanosId = board.id;

          return fngetListById(thanosId);
      })
      .then((lists) => {
          if (!lists) {
              console.log("Lists for Thanos not found");
          }

          thanosLists = lists;

          const mindList = lists.filter((list) => list.name === "Mind" || list.name === "Space");
          const mindListId  = mindList.map((list) => list.id);

          if (!mindList) {
              console.log("Mind list not found in Thanos board");
          }

          const promises = mindListId.map((powerList) => fngetCardsBylist(powerList))
          return Promise.all(promises)
      })
      .then((mindAndSpaceCards) => {
          if (!mindAndSpaceCards) {
              console.log("Cards for Mind and Space list not found");
          }


          // Returning the relevant information
          resolve( {
              thanosBoard: thanosId,
              thanosLists: thanosLists,
              mindAndSpaceListCards: mindAndSpaceCards.flat(),
      });
      })
      .catch((err) => {
          reject(err);
      });
  })
      
}




//create the module to export the function
module.exports = getBoardListandcards;



