// import the fs module with promises
const fs = require("fs").promises;
const problem1 = require("./promise1.cjs");
const problem2 = require("./promise2.cjs");
const problem3 = require("./promise3.cjs");

const boardFilePath = "./boards_1.json";
function problem6() {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      fs.readFile(boardFilePath, "utf8").then((file) => {
        const boardData = JSON.parse(file);
        const thanosBoard = boardData.find(
          (element) => element.name === "Thanos"
        );
        console.log("information from the Thanos boards :", thanosBoard);
        problem2(thanosBoard.id)
          .then((listsObject) => {
            const lists = Object.values(listsObject);
            console.log("All lists for the Thanos board:", lists);
            return lists;
          })
          .then((lists) => {
            const listsId = lists.flat().map((value) => problem3(value.id));
            return Promise.all(listsId);
          })
          .then((cardsArray) => {
            cardsArray.forEach((value) => {
              resolve(value);
            });
          })

          .catch((error) => {
            reject(error);
          });
      });
    }, 3000);
  });
}

//create a module to export the function 
module.exports = problem6;

















