const fngetBoardById = require("./promise1.cjs");
const fngetListsById = require("./promise2.cjs");
const fngetAllCards = require("./promise3.cjs");

// define the function to find Information About Thanos
function getBoardListAndCards() {
    let thanosId;
    let thanosLists;
    let mindListId;

    return new Promise((resolve,reject) => {
        fngetBoardById("mcu453ed")
        .then((board) => {
        //   console.log(board);
            if (!board) {
                reject("Thanos board not found");
            }

            thanosId = board.id;

            return fngetListsById(thanosId);
        })
        .then((lists) => {
            if (!lists) {
                reject("Lists for Thanos not found");
            }

            thanosLists = lists;
                //using the find method find the Mind list Name 
            const mindList = lists.find((list) => list.name === "Mind");

            if (!mindList) {
                console.log("Mind list not found in Thanos board");
                return;
            }

            mindListId = mindList.id;

            return fngetAllCards(mindListId);
        })
        .then((cards) => {
            if (!cards) {
                reject("Cards for Mind list not found");
            }


            resolve( {
                thanosBoard: thanosId,
                thanosLists: thanosLists,
                mindListCards: cards
        });
        })
        .catch((err) => {
            reject(err);
            
        });
    })
        
  }

//create a module to export the function
module.exports = getBoardListAndCards;
   
    