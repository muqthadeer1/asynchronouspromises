// import promises from fs module , import path module
const fs = require("fs").promises;
const path = require("path");

//define the function with argument 
function problem1(id) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            let boardFilePath = path.join(__dirname, "boards_1.json");
            
            fs.readFile(boardFilePath, 'utf8')
                .then((boardsDetail) => {
                    let boards = JSON.parse(boardsDetail);
                    let findBoardById = boards.find((eachBoard) => eachBoard.id == id);
                    
                    if (findBoardById) {
                        resolve(findBoardById);
                    } else {
                        reject(console.log(`Board with ID ${id} not found`));
                    }
                })
                .catch((error) => {
                    reject("error");
                });
        }, 2000);
    });
}


//create a module to export the function 

module.exports = problem1;  


// problem1("abc");  
