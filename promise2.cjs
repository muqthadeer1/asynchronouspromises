//import the fs module and path module in file
const fs = require("fs").promises;
// const { promises } = require("dns");
const path = require("path");

//define the function with one argument
function problem2(id) {
    return new Promise((resolve,reject)=>{
  setTimeout(() => {
    let listPath = path.join(__dirname, "lists_1.json");
    fs.readFile(listPath,'utf-8')
    .then((data)=>{
      let listDetails = JSON.parse(data);
      let getId = listDetails[id];
      if(getId){
        resolve(getId);
      } else {
        reject(console.log("Error Id not found"));
      }
      
    })
    .catch((error)=>{
      reject("Error");
    })
  }, 1000);
});
}


//create the module to export the function to other file
module.exports = problem2;

