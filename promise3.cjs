//import the fs.promises and path modules
const fs = require("fs").promises;
const path = require("path"); 

//define the function with one arguments 
function getAllCards(listId){
    const cardFilePath = path.join(__dirname, "cards_1.json"); 
    return new Promise((resolve,reject) =>{
        setTimeout(() => {
            fs.readFile(cardFilePath,"utf-8")
            .then((data) => {
                const cardsData = JSON.parse(data);
                const cardDetails = cardsData[listId];
                return resolve(cardDetails);
            })
            .catch((err) => {
                console.error("Error in getting cards data for a particulat list",err);
            })
        },2*1000)
    })
}

//create a module to export the function
module.exports = getAllCards; 