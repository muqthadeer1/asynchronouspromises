//import the function from other file
const fnCallback5 = require("../promise5.cjs");

//call function which exported
fnCallback5()
.then((data)=>{
  console.log(data);
})
.catch(error=>{
  console.log(error);
})