//import the function from other file
const fnCallback = require("../promise4.cjs");

//call function which exported
fnCallback()
.then((result) => {
    console.log("Result:", result);
})
.catch((err) => {
    console.error("Error:", err);
});

